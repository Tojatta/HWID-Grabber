package me.tojatta.grabber.ui;

import me.tojatta.grabber.Main;
import me.tojatta.grabber.hwid.HWID;
import me.tojatta.grabber.hwid.HWIDLoader;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Tojatta on 3/3/2016.
 */
public class GrabberUi extends JFrame {

    private JTabbedPane mainTabbedPane;
    private JPanel mainPanel;
    private JButton grabHWIDButton;
    private JComboBox hwidType;
    private HWID currentHwid;

    public GrabberUi(HWIDLoader loader) {
        super("Hardware ID Grabber - Tojatta");
        setContentPane(mainPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        currentHwid = loader.getHwidlist().get(0);
        loader.getHwidlist().stream().forEach(id -> hwidType.addItem(id.getName()));

        grabHWIDButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(Main.ui, String.format("Your HWID is : %s \nAnd was copied to your clipboard.", currentHwid.getHWID()) );
                Toolkit toolkit = Toolkit.getDefaultToolkit();
                Clipboard clipboard = toolkit.getSystemClipboard();
                StringSelection strSel = new StringSelection(currentHwid.getHWID());
                clipboard.setContents(strSel, null);
            }
        });

        hwidType.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentHwid = loader.getHWIDbyName((String)hwidType.getSelectedItem()).get();
            }
        });

        pack();
        setVisible(true);
    }
}
