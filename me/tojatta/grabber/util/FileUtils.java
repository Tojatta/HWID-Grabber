package me.tojatta.grabber.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tojatta on 3/3/2016.
 */
public class FileUtils {

    public static List<File> getFilesInDirectoryByType(File directory, String filetype) {
        List<File> fileList = new ArrayList<>();
        for (String filename : directory.list()) {
            if (filename.toLowerCase().endsWith("." + filetype.trim().toLowerCase())) {
                fileList.add(new File(directory.getAbsolutePath() + File.separator + filename));
            }
        }
        return fileList;
    }
}
