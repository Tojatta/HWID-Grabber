package me.tojatta.grabber.util;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by Tojatta on 3/3/2016.
 */
public class ReflectionUtils {

    public static List<Class> getClassesFromJar(File jarFile) {
        List<Class> classList = new ArrayList<>();
        try {
            @SuppressWarnings("resource")
            ClassLoader classLoader = new URLClassLoader(new URL[]{jarFile.toURI().toURL()}, ReflectionUtils.class.getClassLoader());
            try {
                JarFile jar = new JarFile(jarFile);
                Enumeration jarEnumeration = jar.entries();
                while (jarEnumeration.hasMoreElements()) {
                    JarEntry entry = (JarEntry) jarEnumeration.nextElement();
                    if (entry.isDirectory() | !entry.getName().trim().toLowerCase().endsWith(".class")) {
                        continue;
                    }
                    System.out.println(entry.getName());
                    String className = entry.getName().replace(".class", "").replace("/", ".");
                    try {
                        classList.add(classLoader.loadClass(className));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                jar.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return classList;
    }
}
