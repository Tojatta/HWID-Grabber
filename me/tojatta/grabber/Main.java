package me.tojatta.grabber;

import me.tojatta.grabber.hwid.HWIDLoader;
import me.tojatta.grabber.ui.GrabberUi;

public class Main {

    public static HWIDLoader loader;
    public static GrabberUi ui;

    public static void main(String[] args) {
        loader = new HWIDLoader();
        ui = new GrabberUi(loader);
    }
}
