package me.tojatta.grabber.hwid;

import me.tojatta.grabber.hwid.annotations.HWIDManifest;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Tojatta on 3/3/2016.
 */
public class HWID {

    private String name;
    private String author;
    private String algorithm;
    private String[] properties;
    private String hwid;

    public HWID(String[] properties) {
        this.name = this.getClass().getAnnotation(HWIDManifest.class).idName();
        this.author = this.getClass().getAnnotation(HWIDManifest.class).author();
        this.algorithm = this.getClass().getAnnotation(HWIDManifest.class).algorithm();
        this.properties = properties;
    }

    public String getHWID() {
        hwid = "";
        String props = "";
        for (String e : properties) {
            props += e;
        }
        try {
            MessageDigest md = MessageDigest.getInstance(algorithm);
            md.update(props.getBytes());
            byte[] bytes = md.digest();
            for (int i = 0; i < bytes.length; i++) {
                byte temp = bytes[i];
                String s = Integer.toHexString(new Byte(temp));
                while (s.length() < 2) {
                    s = "0" + s;
                }
                s = s.substring(s.length() - 2);
                hwid += s;
            }
            return hwid;
        } catch (NoSuchAlgorithmException e) {
            System.exit(-1);
        }

        return "";
    }

    public String[] getProperties() {
        return properties;
    }

    public void setProperties(String[] properties) {
        this.properties = properties;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
