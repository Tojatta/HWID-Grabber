package me.tojatta.grabber.hwid.impl;

import me.tojatta.grabber.hwid.HWID;
import me.tojatta.grabber.hwid.annotations.HWIDManifest;

/**
 * Created by Tojatta on 3/3/2016.
 */
@HWIDManifest(idName = "Tojatta HWID - SHA-1", author = "Tojatta", algorithm = "SHA-1")
public class BasicHWID extends HWID {

    public BasicHWID() {
        super(new String[] {
                System.getProperty("os.arch"),
                System.getProperty("os.name"),
                System.getProperty("user.home"),
                System.getProperty("user.name"),
                System.getProperty("user.country"),
                System.getProperty("file.encoding"),
                System.getProperty("sun.cpu.endian"),
                System.getProperty("sun.io.unicode.encoding"),
                System.getProperty("sun.cpu.isalist"),
                System.getProperty("sun.arch.data.model"),
                System.getProperty("os.version")
        });
    }

}
