package me.tojatta.grabber.hwid;

import me.tojatta.grabber.hwid.impl.BaseHWID;
import me.tojatta.grabber.hwid.impl.BasicHWID;
import me.tojatta.grabber.util.FileUtils;
import me.tojatta.grabber.util.ReflectionUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Tojatta on 3/3/2016.
 */
public class HWIDLoader {

    private List<HWID> hwidlist;

    public HWIDLoader() {
        hwidlist = new ArrayList<>();
        loadInternalHwids();
        loadExternalHwids();
    }

    private void loadInternalHwids(){
        hwidlist.add(new BaseHWID());
        hwidlist.add(new BasicHWID());
    }

    private void loadExternalHwids(){
        this.createHWIDJars(new File("." + File.separator + "external-HWIDs")).stream().forEach(hwid -> hwidlist.add(hwid));
    }

    public List<HWID> getHwidlist() {
        return hwidlist;
    }

    public Optional<HWID> getHWIDbyName(String name){
        return hwidlist.stream().filter(id -> id.getName().equalsIgnoreCase(name)).findFirst();
    }

    private List<HWID> createHWIDJars(File jarDirectory) {
        if(!jarDirectory.exists()) {
            jarDirectory.mkdir();
        }
        List<HWID> hList = new ArrayList<>();

        List<File> jars = FileUtils.getFilesInDirectoryByType(jarDirectory, "jar");
        jars.stream().forEach(jar -> this.createHWIDInstanceFromJar(jar).stream().forEach(hwid -> hList.add(hwid)));

        return hList;
    }

    public List<HWID> createHWIDInstanceFromJar(File jarFile) {
        List<HWID> externHWIDs = new ArrayList<>();
        List<Class> jarClassList = ReflectionUtils.getClassesFromJar(jarFile);
        if (jarClassList != null) {
            jarClassList.stream().forEach(aClass -> {
                try {
                    HWID hwid = (HWID)aClass.newInstance();
                    externHWIDs.add(hwid);
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (ClassCastException e) {
                    e.printStackTrace();
                }
            });
        }
        return externHWIDs;
    }
}
