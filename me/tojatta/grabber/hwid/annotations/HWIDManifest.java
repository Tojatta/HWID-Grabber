package me.tojatta.grabber.hwid.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Tojatta on 3/3/2016.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface HWIDManifest {
    String idName();
    String author();
    String algorithm() default "SHA-256";
}
